let http = require("http");

// Mock Database --> where we can save or insert data using variable

let directory = [
	{
		"name" : "Brandon",
		"email" : "brandon@mail.com"
	},
	{
		"name" : "Jobert",
		"email" : "jobertn@mail.com"
	}

]

let port = 4000;

let server = http.createServer((req, res) => {
	//first endpoint "/users" --> HTTP METHOD - GET
	if(req.url == "/users" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "application/json"});
		res.write(JSON.stringify(directory));   //default on Node application
		res.end();
	}

	if(req.url == "/users" && req.method == "POST") {
		// This will act as a placeholder for the resource / data to be created later on

		let requestBody = "";

		req.on("data", function(data){
			// Assigns the data retrieved from the data stream to requestBody
			requestBody += data;
		});

		req.on("data", function(){
			console.log(typeof requestBody);

			requestBody = JSON.parse(requestBody);

			let newUser = {
				"name" : requestBody.name,
				"email" : requestBody.email

			}
			directory.push(newUser);
			console.log(directory);

			res.writeHead(200, {"Content-Type" : "application/json"});
			res.write(JSON.stringify(newUser));
			res.end();

		})

	}

});

server.listen(port);
console.log(`Server is running at localhost:${port}.`);